package HT9;

public class Main {
    public static void main(String[] args) {
        int number = 12345;
        NumberProcess numberProcess = number1 -> {
            StringBuilder sb = new StringBuilder(String.valueOf(number1));
            String s = sb.reverse().toString();
            return Integer.parseInt(s);
        };
        System.out.println(numberProcess.process(number));
    }
}
