package HT9;
@FunctionalInterface
public interface NumberProcess {
    int process(int number);
}
