import java.util.*;

public class HT42 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        System.out.println("Inter digits:");
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        int first = 0;
        int last = array.length - 1;

        bubble(array);
        System.out.println(Arrays.toString(array));

        System.out.println("Введите элемент для бинарного поиска: ");
        int find = scanner.nextInt();

        int index = binarySearch(array, first, last, find);
        index = Arrays.binarySearch(array, find);
        System.out.println(index);
    }

    public static void bubble(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }

    public static int binarySearch(int[] array, int first, int last, int find) {
        int index = (first + last) / 2;
        if (last >= first) {
            if (array[index] == find) {
                return index;
            }
            if (array[index] > find) {
                return binarySearch(array, first, index - 1, find);
            }
            return binarySearch(array, index + 1, last, find);
        }
        return -1;
    }
}
