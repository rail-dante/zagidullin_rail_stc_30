package HT8;

public class Tank extends MilitaryTransport {

    private double angle;

    public Tank(double fuelAmount, double fuelConsumption, int bulletsCount, double angle) {
        super(fuelAmount,fuelConsumption, bulletsCount);

        this.angle = angle;
    }
    public void stop() {
        System.out.println("Stop vichicle!");
    }

    public void round() {
        if (this.angle > -1)
        {
        this.angle -= 0.1;
        } else if (this.angle < 1) {
            this.angle += 0.1;
        }
    }
}
