package HT8;

public abstract class Transport {
    protected double fuelAmount;
    protected double fuelConsumption;

    public Transport(double fuelAmount, double fuelConsumption) {
        this.fuelAmount = fuelAmount;
        this.fuelConsumption = fuelConsumption;
    }
    public abstract void stop();


    public void go(int km) {
        this.fuelAmount -= (km  * fuelConsumption) / 100;
    }


    public double getFuelAmount() {
        return fuelAmount;
    }
}
