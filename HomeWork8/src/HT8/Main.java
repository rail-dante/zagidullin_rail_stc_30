package HT8;

public class Main {

    public static void main(String[] args) {
        Tank tank = new Tank(100, 100, 10, 0.5);
        tank.go(5);
        tank.fire();
        tank.fire();
        tank.fire();
        System.out.println(tank.getFuelAmount());
        System.out.println(tank.getBulletsCount());

        Airplane airplane = new Airplane(350, 4, 30);
        airplane.go(10);
        System.out.println(airplane.getFuelAmount());

        //HT8.Transport transport = new HT8.Transport(100,10);
        //HT8.MilitaryTransport militaryTransport = new HT8.MilitaryTransport(10,5, 5);
        Transport transport = airplane;
        MilitaryTransport militaryTransport = tank;

        //airplane.stop();
        //tank.stop();

        transport.go(100);
        militaryTransport.stop();
    }

}

