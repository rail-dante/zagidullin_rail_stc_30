package HT8;

public class Airplane extends Transport {

    private double length;

    public Airplane(double fuelAmount, double fuelConsumption, double length) {
        super(fuelAmount, fuelConsumption);
        this.length = length;
    }

    @Override
    public void go(int km) {
        System.out.println("I am plane, I am flying!");
        this.fuelAmount -= (km * fuelConsumption) / 100;
        this.fuelConsumption = -0.1;
    }
    @Override
    public void stop() {
        System.out.println("landing!");
    }

    public double getLength() {
        return length;
    }
}
