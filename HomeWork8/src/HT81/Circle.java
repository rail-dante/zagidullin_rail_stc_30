package HT81;

public class Circle extends GeometricalFigure {
    private double a;
    public double area;
    public double perimeter;

    public Circle(double CenterX, double CenterY, double a) {
        super(CenterX, CenterY);
        this.a = a;
        this.area = area;
        this.perimeter = perimeter;
    }

    public void perimeter(double m) {
        this.perimeter = 4 * Math.PI * a;
    }

    public double getA() {
        return a;
    }


    @Override
    public double getArea() {
        return area = Math.PI * a * a;

    }

    @Override
    public double getPerimeter() {

        return perimeter = 2 * Math.PI * a;
    }

    @Override
    public void move(double dx, double dy) {
        this.centerX += dx;
        this.centerY += dy;

    }

    @Override
    public void scale(double k) {
        this.a *= k;

    }
}
