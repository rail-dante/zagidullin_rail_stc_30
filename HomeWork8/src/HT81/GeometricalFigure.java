package HT81;

public abstract class GeometricalFigure implements Moveable, Scalable {
    protected double centerX;
    protected double centerY;



    public GeometricalFigure(double centerX, double centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }
    public abstract double getArea();
    public abstract double getPerimeter();

}
