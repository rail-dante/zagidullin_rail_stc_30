package HT81;

public interface Moveable {
    public void move(double dx, double dy);

}
