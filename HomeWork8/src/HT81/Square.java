package HT81;

public class Square extends Rectangle {
    public Square(double centerX, double centerY, double a, double b) {

        super(centerX, centerY, a, b);
        b = a;
        this.a = a;
        this.b = b;


    }

    @Override
    public double getArea() {
        return a * a;
    }

    @Override
    public double getPerimeter() {
        return 4 * a;
    }
}
