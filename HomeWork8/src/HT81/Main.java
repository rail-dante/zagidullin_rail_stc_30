package HT81;

public class Main {
    public static void main(String[] args) {
        GeometricalFigure circle = new Circle(10, 10, 10);
        circle.move(5, 5);
        circle.scale(2);

        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());

        GeometricalFigure ellipse = new Ellipse(10, 10, 20, 30);
        ellipse.move(5, 5);
        ellipse.scale(2);

        System.out.println(ellipse.getArea());
        System.out.println(ellipse.getPerimeter());

        GeometricalFigure rectangle = new Rectangle(10, 10, 10, 10);
        rectangle.move(100, 100);
        rectangle.scale(3);

        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());

        GeometricalFigure square = new Square(10, 10, 10, 10);
        square.move(2, 2);
        square.scale(4);

        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());

    }

}
