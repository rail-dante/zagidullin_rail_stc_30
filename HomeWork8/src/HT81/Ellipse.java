package HT81;


public class Ellipse extends Circle {
    private double a;
    private double b;
    public double area;
    public double perimeter;

    public Ellipse(double centerX, double centerY, double a, double b) {
        super(centerX, centerY, a);
        this.a = a;
        this.b = b;
        this.area = area;
        this.perimeter = perimeter;
    }

    public void area(double m2) {
        this.area = Math.PI * a * b;
    }

    public void perimeter(double m) {
        this.perimeter = 4 * ((Math.PI * a * b + (a-b))/ (b + a));
    }

    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }

    public double getArea() {
        return Math.PI * a * b;
    }

    public double getPerimeter() {
        return 4 * ((Math.PI * a * b + (a-b))/ (b + a));
    }
}
