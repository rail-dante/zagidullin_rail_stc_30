package HT81;

public class Rectangle extends GeometricalFigure {
    protected double a;
    protected double b;

    public Rectangle(double centerX, double centerY, double a, double b) {
        super(centerX, centerY);
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    @Override
    public double getArea() {
        return a * b;
    }

    @Override
    public double getPerimeter() {
        return 2 * (a + b);
    }

    @Override
    public void move(double dx, double dy) {
        this.centerX += dx;
        this.centerY += dy;

    }

    @Override
    public void scale(double k) {
        this.a *= k;
        this.b *= k;

    }
}
