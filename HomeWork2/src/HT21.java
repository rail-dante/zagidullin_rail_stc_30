import java.util.*;

public class HT21 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        int sum = 0;
        System.out.println("Inter digits:");
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        for (int num : array) {
            sum += num;
        }
        System.out.println("Summa = : " + sum);
    }
}
	