import java.util.*;

public class HT22 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        System.out.println("Inter digits:");
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }
}