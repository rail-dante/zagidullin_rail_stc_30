import java.util.*;

public class HT24 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        System.out.println("Inter digits:");
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }

            array[indexOfMin] = array[i];
            array[i] = min;
        }
        System.out.println(Arrays.toString(array));
    }

}

