public class MyNewClass {

    private int size = 8;

    MyNewClass(int size) {
        this.size = size;
    }

    void printBinary(long value) {
        for(int i = size - 1; i >= 0; i--) {
            long mask = 1L << i;
            long result = (mask & value) >> i;
            System.out.print(result);
        }
    }

    int getSize() {
        return size;
    }
}
