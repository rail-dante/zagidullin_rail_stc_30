

public class Main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Rail")
                .lastName("Zagidullin")
                .age(35)
                .isWorker(true)
                .build();

        System.out.println(user);
    }
}
