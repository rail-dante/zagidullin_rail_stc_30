
public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    public static class Builder {
        private final User newUser;

        public Builder() {
            this.newUser = new User();
        }

        public Builder firstName(String firstName) {
            newUser.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            newUser.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            newUser.isWorker = isWorker;
            return this;
        }

        public User build() {
            return newUser;
        }
    }
}
