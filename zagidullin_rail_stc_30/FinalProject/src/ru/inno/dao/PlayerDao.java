package ru.inno.dao;

import ru.inno.models.Player;

public interface PlayersDao extends CrudDao<Player> {
}
