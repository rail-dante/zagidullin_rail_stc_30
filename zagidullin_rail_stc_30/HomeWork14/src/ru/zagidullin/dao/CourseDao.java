package ru.zagidullin.dao;

import ru.zagidullin.models.Course;

public interface CourseDao extends CrudDao<Course> {

}
