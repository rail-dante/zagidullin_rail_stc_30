package ru.zagidullin.dao;

import ru.zagidullin.models.Course;
import ru.zagidullin.utils.IdGenerator;
import ru.zagidullin.utils.IdGeneratorFileBasedImpl;
import ru.zagidullin.utils.Mapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl implements CourseDao {
    private final String fileName;
    private final Mapper<String, Course> stringToCourseMapper;
    private final IdGenerator idGenerator;

    public CourseDaoImpl(String fileName, Mapper<String, Course> stringToCourseMapper) {
        this.fileName = fileName;
        this.stringToCourseMapper = stringToCourseMapper;
        this.idGenerator = new IdGeneratorFileBasedImpl("course_sequence.txt");
    }

    @Override
    public List<Course> findAll() {
        try {
            // создаем пустой список
            List<Course> courses = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String line = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (line != null) {
                // line = 0 Молоко 50.5 10
                // преобразуем по какому-то правилу строку в объект типа Course
                Course course = stringToCourseMapper.map(line);
                // добавляем этого пользователя в список
                courses.add(course);
                // считываем следующую строку в файле
                line = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Course course) {
        try {
            course.setId(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(course.toString().getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
