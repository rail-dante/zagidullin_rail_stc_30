package ru.zagidullin.dao;

import ru.zagidullin.models.Teacher;
public interface TeacherDao extends CrudDao<Teacher> {

}
