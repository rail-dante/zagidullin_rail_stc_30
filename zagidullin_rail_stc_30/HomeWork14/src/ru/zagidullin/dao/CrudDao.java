package ru.zagidullin.dao;

import java.util.List;

public interface CrudDao<T> {
    List<T> findAll(); // SELECT

    void save(T t); // INSERT

    // void update(T entity);
    // void delete(T entity);
    // Optional<T> findById(Long id);
}
