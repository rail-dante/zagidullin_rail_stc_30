package ru.zagidullin.dao;

import ru.zagidullin.models.Lesson;
import ru.zagidullin.utils.IdGenerator;
import ru.zagidullin.utils.IdGeneratorFileBasedImpl;
import ru.zagidullin.utils.Mapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LessonDaoImpl implements LessonDao {
    private final String fileName;
    private final Mapper<String, Lesson> stringToLessonMapper;
    private final IdGenerator idGenerator;

    public LessonDaoImpl(String fileName, Mapper<String, Lesson> stringToLessonMapper) {
        this.fileName = fileName;
        this.stringToLessonMapper = stringToLessonMapper;
        this.idGenerator = new IdGeneratorFileBasedImpl("lesson_sequence.txt");
    }

    @Override
    public List<Lesson> findAll() {
        try {
            // создаем пустой список
            List<Lesson> lessons = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String line = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (line != null) {
                // line = 0 Молоко 50.5 10
                // преобразуем по какому-то правилу строку в объект типа Lesson
                Lesson lesson = stringToLessonMapper.map(line);
                // добавляем этого пользователя в список
                lessons.add(lesson);
                // считываем следующую строку в файле
                line = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Lesson lesson) {
        try {
            lesson.setId(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(lesson.toString().getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
