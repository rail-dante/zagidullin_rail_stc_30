package ru.zagidullin.dao;

import ru.zagidullin.models.Lesson;

public interface LessonDao extends CrudDao<Lesson> {
}
