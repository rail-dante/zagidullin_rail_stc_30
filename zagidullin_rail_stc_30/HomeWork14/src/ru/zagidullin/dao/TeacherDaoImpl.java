package ru.zagidullin.dao;

import ru.zagidullin.models.Teacher;
import ru.zagidullin.utils.IdGenerator;
import ru.zagidullin.utils.IdGeneratorFileBasedImpl;
import ru.zagidullin.utils.Mapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherDaoImpl implements TeacherDao {
    private final String fileName;
    private final Mapper<String, Teacher> stringToTeacherMapper;
    private final IdGenerator idGenerator;

    public TeacherDaoImpl(String fileName, Mapper<String, Teacher> stringToTeacherMapper) {
        this.fileName = fileName;
        this.stringToTeacherMapper = stringToTeacherMapper;
        this.idGenerator = new IdGeneratorFileBasedImpl("teacher_sequence.txt");
    }

    @Override
    public List<Teacher> findAll() {
        try {
            // создаем пустой список
            List<Teacher> teachers = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String line = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (line != null) {

                // преобразуем по какому-то правилу строку в объект типа Teacher
                Teacher teacher = stringToTeacherMapper.map(line);
                // добавляем этого пользователя в список
                teachers.add(teacher);
                // считываем следующую строку в файле
                line = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Teacher teacher) {
        try {
            teacher.setId(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(teacher.toString().getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
