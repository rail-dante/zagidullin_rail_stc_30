package ru.zagidullin;

import ru.zagidullin.services.CourseService;
import ru.zagidullin.services.LessonService;
import ru.zagidullin.services.TeacherService;

public class Main {
    private static final CourseService courseService = new CourseService();
    private static final LessonService lessonService = new LessonService();
    private static final TeacherService teacherService = new TeacherService();

    public static void main(String[] args) {

        courseService.showFunctionality();
        lessonService.showFunctionality();
        teacherService.showFunctionality();
    }
}
