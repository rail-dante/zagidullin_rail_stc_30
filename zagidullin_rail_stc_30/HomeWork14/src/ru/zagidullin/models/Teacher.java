package ru.zagidullin.models;

import java.time.Period;
import java.util.List;
import java.util.Objects;

public class Teacher {
    private Long id;
    private String firstName;
    private String lastName;
    private Period experience;
    private List<Course> courses;

    public Teacher() {
    }

    public Teacher(String firstName, String lastName, Period experience, List<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.courses = courses;
    }

    public Teacher(Long id, String firstName, String lastName, Period experience, List<Course> courses) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.courses = courses;
    }

    public Teacher(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", experience=" + experience +
                ",\ncourses=" + courses +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(id, teacher.id) &&
                Objects.equals(firstName, teacher.firstName) &&
                Objects.equals(lastName, teacher.lastName) &&
                Objects.equals(experience, teacher.experience) &&
                Objects.equals(courses, teacher.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, experience, courses);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Period getExperience() {
        return experience;
    }

    public List<Course> getCourses() {
        return courses;
    }
}
