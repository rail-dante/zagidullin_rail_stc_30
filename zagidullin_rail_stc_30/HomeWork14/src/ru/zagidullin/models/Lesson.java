package ru.zagidullin.models;

import java.time.LocalDateTime;

public class Lesson {
    private Long id;
    private LocalDateTime lessonDateTime;
    private String name;
    private Course course;

    public Lesson() {
    }

    public Lesson(String name) {
        this.name = name;
    }

    public Lesson(LocalDateTime lessonDateTime, String name, Course course) {
        this.lessonDateTime = lessonDateTime;
        this.name = name;
        this.course = course;
    }

    public Lesson(Long id, LocalDateTime lessonDateTime, String name, Course course) {
        this.id = id;
        this.lessonDateTime = lessonDateTime;
        this.name = name;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", lessonDateTime=" + lessonDateTime +
                ", name='" + name + '\'' +
                ",\ncourse=" + course +
                '}';
    }
    public void setId(Long id) {
        this.id = id;
    }
}
