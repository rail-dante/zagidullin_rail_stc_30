package ru.zagidullin.models;

import java.time.LocalDate;
import java.util.List;

public class Course {
    private Long id;
    private String name;
    private LocalDate beginDate;
    private LocalDate endDate;
    private List<Teacher> teachers;
    private List<Lesson> lessons;

    public Course(String name, LocalDate beginDate, LocalDate endDate, List<Teacher> teachers, List<Lesson> lessons) {
        this.name = name;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    public Course(Long id, String name, LocalDate beginDate, LocalDate endDate, List<Teacher> teachers, List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    public Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ",\nteachers=" + teachers +
                ",\nlessons=" + lessons +
                '}';
    }
    public void setId(Long id) {
        this.id = id;
    }
}
