package ru.zagidullin.services;

import ru.zagidullin.dao.LessonDao;
import ru.zagidullin.dao.LessonDaoImpl;
import ru.zagidullin.models.Course;
import ru.zagidullin.models.Lesson;
import ru.zagidullin.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

import static java.lang.Long.parseLong;

public class LessonService {
    public final LessonDao lessonDao = new LessonDaoImpl("lessons.txt", Utils::parseLesson);

    public void showFunctionality() {
        Lesson lesson;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("inputLesson.txt"))) {
            String line;
            lesson = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                lesson = new Lesson(parseLong(data[0]), LocalDateTime.parse(data[1]), data[2],
                        new Course(data[3]));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        System.out.println(lessonDao.findAll());

        lessonDao.save(lesson);
    }
}


