package ru.zagidullin.services;

import ru.zagidullin.dao.TeacherDao;
import ru.zagidullin.dao.TeacherDaoImpl;
import ru.zagidullin.models.Course;
import ru.zagidullin.models.Teacher;
import ru.zagidullin.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Period;

import static java.util.Collections.singletonList;

public class TeacherService {
    private final TeacherDao teacherDao = new TeacherDaoImpl("teachers.txt", Utils::parseTeacher);

    public void showFunctionality() {
        Teacher teacher;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"))) {
            String line;
            teacher = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                teacher = new Teacher(Long.parseLong(data[0]), data[1], data[2],
                        Period.parse(data[3]), singletonList(new Course(data[4])));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        System.out.println(teacherDao.findAll());

        teacherDao.save(teacher);
    }
}
