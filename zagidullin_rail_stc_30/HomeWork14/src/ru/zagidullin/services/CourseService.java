package ru.zagidullin.services;

import ru.zagidullin.dao.CourseDao;
import ru.zagidullin.dao.CourseDaoImpl;
import ru.zagidullin.models.Course;
import ru.zagidullin.models.Lesson;
import ru.zagidullin.models.Teacher;
import ru.zagidullin.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Long.parseLong;
import static java.time.LocalDate.parse;
import static java.util.Collections.singletonList;

public class CourseService {
    private final CourseDao courseDao = new CourseDaoImpl("courses.txt", Utils::parseCourse);

    public void showFunctionality() {
        Course course;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("inputCourse.txt"))) {
            String line;
            course = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                course = new Course(parseLong(data[0]), data[1], parse(data[2]),
                        parse(data[3]), singletonList(new Teacher(data[4])), singletonList(new Lesson(data[5])));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        System.out.println(courseDao.findAll());

        courseDao.save(course);
    }
}
