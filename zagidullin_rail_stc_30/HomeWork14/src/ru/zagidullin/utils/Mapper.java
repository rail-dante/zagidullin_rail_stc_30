package ru.zagidullin.utils;

@FunctionalInterface
public interface Mapper<X, Y> {
    Y map(X x);
}
