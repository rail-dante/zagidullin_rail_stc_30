package ru.zagidullin.utils;

@FunctionalInterface
public interface IdGenerator {
    Long nextId();
}
