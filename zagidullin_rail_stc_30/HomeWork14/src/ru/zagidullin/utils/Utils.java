package ru.zagidullin.utils;

import ru.zagidullin.models.Course;
import ru.zagidullin.models.Lesson;
import ru.zagidullin.models.Teacher;

import java.time.LocalDateTime;
import java.time.Period;

import static java.lang.Long.parseLong;
import static java.time.LocalDate.parse;
import static java.util.Collections.singletonList;

public class Utils {
    public static Teacher parseTeacher(String string) {
        String[] strings = string.split("\\s+");
        return new Teacher(parseLong(strings[0]),
                strings[1], strings[2], Period.parse(strings[3]),
                singletonList(new Course(strings[4])));
    }

    public static Lesson parseLesson(String string) {
        String[] strings = string.split("\\s+");
        return new Lesson(parseLong(strings[0]),
                LocalDateTime.parse(strings[1]), strings[2], new Course(strings[3]));
    }

    public static Course parseCourse(String string) {
        String[] strings = string.split("\\s+");
        return new Course(parseLong(strings[0]), strings[1],
                parse(strings[2]), parse(strings[3]),
                singletonList(new Teacher(strings[4])), singletonList(new Lesson(strings[5])));
    }
}
