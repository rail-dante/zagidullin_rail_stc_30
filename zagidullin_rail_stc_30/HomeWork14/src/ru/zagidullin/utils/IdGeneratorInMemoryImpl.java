package ru.zagidullin.utils;

public class IdGeneratorInMemoryImpl implements IdGenerator {
    private Long lastGeneratedId;

    public IdGeneratorInMemoryImpl() {
        this.lastGeneratedId = -1L;
    }

    @Override
    public Long nextId() {
        return ++this.lastGeneratedId;
    }
}
