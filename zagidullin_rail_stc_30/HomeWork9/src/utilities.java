import static java.lang.Character.isDigit;

public class utilities {

    static int reverseDigits(int number) {
        return Integer.parseInt(new StringBuilder(String.valueOf(number)).reverse().toString());
    }

    static int makeOddDigitsEven(int number) {
        String string = String.valueOf(number);
        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] % 2 != 0) {
                chars[i]--;
            }
        }
        return Integer.parseInt(new String(chars));
    }

    static String reversString(String process) {
        StringBuilder strRev = new StringBuilder();
        return strRev.reverse().toString();
    }

    static String removeAllDigits(String process) {
        StringBuilder strWithoutInt = new StringBuilder();
        char[] temp = process.toCharArray();
        for (char c : temp) {
            if (!isDigit(c)) {
                strWithoutInt.append(c);
            }
        }
        return strWithoutInt.toString();
    }
}
