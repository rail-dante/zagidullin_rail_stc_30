import java.util.Arrays;

public class NumberAndStringProcessor {
    protected int[] intSequence;
    protected String[] stringSequence;

    public NumberAndStringProcessor(int[] intSequence, String[] stringSequence) {
        this.intSequence = new int[intSequence.length];
        this.stringSequence = new String[stringSequence.length];
        System.arraycopy(intSequence, 0, this.intSequence, 0, intSequence.length);
        System.arraycopy(stringSequence, 0, this.stringSequence, 0, stringSequence.length);

    }

    public NumberAndStringProcessor intProcessor(NumbersProcess numbersProcess) {
        for (int i = 0; i < intSequence.length; i++) {
            this.intSequence[i] = numbersProcess.process(intSequence[i]);
        }
        return this;
    }

    public NumberAndStringProcessor stringProcessor(StringsProcess stringsProcess) {
        for (int i = 0; i < stringSequence.length; i++) {
            this.stringSequence[i] = stringsProcess.process(stringSequence[i]);

        }
        return this;
    }

    public void integerArrayPrinter() {
        System.out.println(Arrays.toString(this.intSequence));
    }

    public void stringArrayPrinter() {
        System.out.println(Arrays.toString(this.stringSequence));
    }
}
