public class Main {

    public static void main(String[] args) {
        int[] intArray = {3, 543, 2342, 10032,
                0, 445, 99, 1003};

        String[] strArray = {"Yt1L", "diSD3", "Kro0W!"};

        NumberAndStringProcessor someNumbersAndString = new NumberAndStringProcessor(intArray, strArray);

        System.out.println("Primal array: ");
        someNumbersAndString.integerArrayPrinter();

        // revers digits;
        int number = 12345;
        NumbersProcess reverseDigits = utilities::reverseDigits;
        NumbersProcess makeOddDigitsEven = utilities::makeOddDigitsEven;
        System.out.println(reverseDigits.process(number));
        System.out.println(makeOddDigitsEven.process(number));


        //remove zeroes
//        someNumbersAndString.intProcessor(Main::removeZeroesNumber);
//        System.out.println("Output without Zeroes ");
//        someNumbersAndString.integerArrayPrinter();


        // revers string
        someNumbersAndString.stringProcessor(utilities::reversString);
        someNumbersAndString.stringArrayPrinter();

        // delete all digits
        someNumbersAndString.stringProcessor(utilities::removeAllDigits);
        someNumbersAndString.stringArrayPrinter();

        //upper Case
        someNumbersAndString.stringProcessor(String::toUpperCase);
        someNumbersAndString.stringArrayPrinter();
    }
}
