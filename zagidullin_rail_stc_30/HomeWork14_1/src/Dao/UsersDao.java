package Dao;

import Models.User;

import java.io.FileNotFoundException;

public interface UserDao extends CrudDao<User>{
    User findByFirstName(String firstName);

}
