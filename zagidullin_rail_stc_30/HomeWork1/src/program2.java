public class program2 {
    public static int fact(int n) {
        System.out.println(n);
        if (n == 0) {
            return 1;
        }
        return fact(n-1)  * n;
    }
    public static void main(String[] args) {
        int number = fact(5);
        System.out.println(number);
    }
}
