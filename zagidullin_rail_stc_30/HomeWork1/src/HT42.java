import java.util.*;

public class HT42 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        System.out.println("Inter digits:");
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        int first = array[0];
        int last = array.length - 1;
        //System.out.println(array.length);

        bubble(array);
        System.out.println(Arrays.toString(array));

        System.out.println("Введите элемент для бинарного поиска: ");
        int find = scanner.nextInt();

        binarySearch(array, first, last, find);

        //int number = function(5);
        //System.out.println(number);
    }


    public static void bubble(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }

    public static void binarySearch(int[] array, int first, int last, int find) {
        int index;
        index = (first + last) / 2;
        while ((array[index] != find) && (first <= last)) {
            if (array[index] > find) {
                last = index - 1;
            } else {
                first = index + 1;
            }
            index = (first + last) / 2;
        }
        if (first <= last) {
            System.out.println(find + "is: " + index + "elemends array");
        } else {
            System.out.println("Elemends is not find");
        }
    }



    /*public static int binarySearch(int n) {
        if (n == 0) {
            return 1;
        }
        return function(n - 1) * n;
    }*/
}