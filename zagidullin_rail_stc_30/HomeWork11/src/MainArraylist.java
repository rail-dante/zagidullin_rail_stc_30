import java.util.ArrayList;

public class MainArraylist {
    public static void main(String[] args) {

        ArrayList<Person> list = new ArrayList();

        //ArrayList<String> aListNumbers
        //                = new ArrayList<String>();

        Person person1 = new Person("Rail");
        Person person2 = new Person("Faim");
        Person person3 = new Person("Egor");

        list.add(person1);
        list.add(person2);
        list.add(person3);

        System.out.println(list);

        list.add(0, person3);

        System.out.println(list);

        list.remove(0);
        System.out.println(list);

    }
}
