import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class MainLinkedList {

    public static void main(String[] args) {

        LinkedList<Person> list = new LinkedList<>();

        Person person1 = new Person("Rail");
        Person person2 = new Person("Faim");
        Person person3 = new Person("Egor");

        list.add(person1);
        list.add(person2);
        list.add(person3);

        //revers list
        Collections.reverse(list);
        System.out.print("\nRevers list: " + list);
        System.out.println();

        //removeFirst
        list.removeFirst();
        System.out.println(list);

        //contains
        System.out.println(list.contains(person2));

        //insert
        list.add(1, person1);
        System.out.println(list);

        //removeByIndex
        list.remove(1);
        System.out.println(list);

        //iterator
        Iterator<Person> iterator = list.iterator();

        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();

        }

    }
}
