import java.util.*;

public class HT13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int product = 1;
        while (number != 0) {
            int sum = 0;
            int copy = number;
            final int REMAINS = 10;
            while (copy != 0) {
                sum += copy % REMAINS; // 9, 1, 1
                copy /= REMAINS; //11, 1, 0
            }
            boolean isPrime = true;
            for (int i = 2; i < sum; i++) {
                if (sum % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                product *= number;
            }
            number = scanner.nextInt();
        }
        System.out.println(product);
    }
}