public class HT12 {

    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        if (number >= 10_000 && number < 100_000) {
            System.out.println(Integer.toBinaryString(number));
        } else {
            System.out.println("Вы ввели не пятизначное чиcло");
        }
    }
}
