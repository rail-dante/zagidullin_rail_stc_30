public class HT11 {
    public static void main(String[] args) {
        int number = 12345;
        int sum = 0;
        final int REMAINS = 10;

        sum = sum + number % REMAINS; // 5
        number = number / REMAINS;
        sum = sum + number % REMAINS; // 4
        number = number / REMAINS;
        sum = sum + number % REMAINS; // 3
        number = number / REMAINS;
        sum = sum + number % REMAINS; // 2
        number = number / REMAINS;
        sum = sum + number % REMAINS; // 1
        System.out.println(sum);
    }
}