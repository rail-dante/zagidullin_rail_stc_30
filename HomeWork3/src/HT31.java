import java.util.*;

public class HT31 {
    public static void main(String args[]) {
        System.out.println("Inter digits:");
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int sum = sum(array);
        System.out.println("Sum = : " + sum);

        turn(array);

        double arrayAverage = arrayAverage(array);
        System.out.println("arrayAverage = : " + arrayAverage);

        swap(array);

        System.out.println(Arrays.toString(array));

        bubble(array);
        System.out.println(Arrays.toString(array));

        // Gorner
        int number = digit(array);
        System.out.println(number);

    }

    public static int sum(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        return sum;
    }

    public static void turn(int[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }

    public static double arrayAverage(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        double arrayAverage = sum / array.length;
        return arrayAverage;
    }

    public static void swap(int[] array) {
        int min = array[0];
        int max = array[0];
        int indexOfMin = 0;
        int indexOfMax = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                indexOfMin = i;
            }
            if (array[i] > max) {
                max = array[i];
                indexOfMax = i;
            }
        }
        array[indexOfMin] = max;
        array[indexOfMax] = min;

    }


    public static void bubble(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }

    public static int digit(int[] array) {
        int number = 0;
        for (int i = 1; i < array.length; i++) {
            number = number * 10 + array[i];
        }
        return number;
    }
}
