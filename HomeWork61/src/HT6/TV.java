package HT6;

public class TV {
    private RemoteControler remoteControler;
    private final Channel[] channels;

    private final String model;
    private Channel activeChannel;

    public TV(String model, Channel[] channels) {
        this.model = model;
        this.channels = channels;
    }

    public void connectTo(RemoteControler remoteControler) {
        this.remoteControler = remoteControler;
        this.remoteControler.setTv(this);
    }

    public Program switchChannel(int number){
        this.activeChannel = channels[--number];
        return this.activeChannel.showRandomProgram();

    }
}
