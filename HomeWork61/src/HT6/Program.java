package HT6;

public class Program {
    private final String name;

    public Program(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Program{" +
                "name='" + name + '\'' +
                '}';
    }
}
