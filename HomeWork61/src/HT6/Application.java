package HT6;

import java.sql.SQLOutput;

public class Application {
    public static void main(String[] args) {
        Channel[] channels = {new Channel("First"), new Channel("Second"), new Channel("Ephir")};
        TV tv = new TV("Samsung", channels);
        RemoteControler remoteControler = new RemoteControler();
        tv.connectTo(remoteControler);

        Program[][] programs = {
                {new Program("Good morning"), new Program("Time"), new Program("Loto")},
                {new Program("Judgment"), new Program("Hi Andrey"), new Program("Me")},
                {new Program("Film 1"), new Program("Film 2"), new Program("Film 3")}
        };

        channels[0].setPrograms(programs[0]);
        channels[1].setPrograms(programs[1]);
        channels[2].setPrograms(programs[2]);

        System.out.println(remoteControler.switchChannel(1));
        System.out.println(remoteControler.switchChannel(2));
        System.out.println(remoteControler.switchChannel(3));
    }
}
