package HT6;

    public class RemoteControler {
        private TV tv;

        public void setTv(TV tv) {
            this.tv = tv;
        }

        public Program switchChannel(int number) {
            return tv.switchChannel(number);
        }
    }
