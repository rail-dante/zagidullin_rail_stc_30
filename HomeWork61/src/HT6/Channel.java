package HT6;

import java.util.Random;

public class Channel {
    private Program[] programs;

    private final String name;

    public Channel(String name) {
        this.name = name;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    public Program showRandomProgram() {
        Random random = new Random();
        int nextInt = random.nextInt(3);
        return programs[nextInt];

    }
}
