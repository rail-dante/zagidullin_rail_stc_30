package HT7;

public class Main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Rail")
                .lastName("Zagidullin")
                .age(36)
                .isWorker(true)
                .build();

        System.out.println(user);
    }
}
